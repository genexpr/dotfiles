alias k=kubectl
alias python=python3
alias pip=pip3
alias penv='source venv/bin/activate'
alias cloud='cd ~/Library/Mobile\ Documents/com~apple~CloudDocs'

export EDITOR=vim
export PS1='%1~ %* %# '

#export GOPATH=$(go env GOPATH)
#export GOBIN=$GOPATH/bin
#export PATH=$PATH:$GOBIN:~/programs

function abspath {
    echo "$(cd "$(dirname "$1")"; pwd -P)/$(basename "$1")"
}

export PATH="/opt/homebrew/opt/ruby/bin:$PATH"
export ERL_AFLAGS="-kernel shell_history enabled"
